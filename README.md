# Go rpc

This is a simple example of how to use rpc in go.
```shell
microservice auth:

https://gitlab.com/citaces/grpc-auth

microservice users:

https://gitlab.com/citaces/grpc-users

microservice exchange:

https://gitlab.com/citaces/grpc-exchange

docker-compose up --build

```

