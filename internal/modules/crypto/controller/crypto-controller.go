package controller

import (
	"net/http"

	"gitlab.com/citaces/grpc-gateway/internal/infrastructure/handlers"

	"gitlab.com/citaces/grpc-gateway/internal/infrastructure/errors"
	"gitlab.com/citaces/grpc-gateway/internal/modules/crypto/service"

	"github.com/ptflp/godecoder"
	"gitlab.com/citaces/grpc-gateway/internal/infrastructure/component"
	"gitlab.com/citaces/grpc-gateway/internal/infrastructure/responder"
)

type Crypter interface {
	Cost(http.ResponseWriter, *http.Request)
	History(http.ResponseWriter, *http.Request)
	Max(http.ResponseWriter, *http.Request)
	Min(http.ResponseWriter, *http.Request)
	Avg(http.ResponseWriter, *http.Request)
}

type Crypto struct {
	crypto service.Crypter
	responder.Responder
	godecoder.Decoder
}

func NewCrypto(crypto service.Crypter, components *component.Components) Crypter {
	return &Crypto{
		crypto:    crypto,
		Responder: components.Responder,
		Decoder:   components.Decoder,
	}
}

func (c *Crypto) Cost(writer http.ResponseWriter, request *http.Request) {
	_, err := handlers.ExtractUser(request)
	if err != nil {
		c.ErrorBadRequest(writer, err)
		return
	}
	out := c.crypto.Cost(request.Context())
	if out.ErrorCode != errors.NoError {
		c.OutputJSON(writer, CryptoResponse{
			ErrorCode: out.ErrorCode,
			Data: Data{
				Message: "retrieving user error",
			},
		})
		return
	}

	c.OutputJSON(writer, CryptoResponse{
		Success:   true,
		ErrorCode: out.ErrorCode,
		Data: Data{
			Crypto: out.Coins,
		},
	})
}

func (c *Crypto) History(writer http.ResponseWriter, request *http.Request) {
	_, err := handlers.ExtractUser(request)
	if err != nil {
		c.ErrorBadRequest(writer, err)
		return
	}
	out := c.crypto.History(request.Context())
	if out.ErrorCode != errors.NoError {
		c.OutputJSON(writer, CryptoResponse{
			ErrorCode: out.ErrorCode,
			Data: Data{
				Message: "retrieving user error",
			},
		})
		return
	}

	c.OutputJSON(writer, CryptoResponse{
		Success:   true,
		ErrorCode: out.ErrorCode,
		Data: Data{
			Crypto: out.Coins,
		},
	})
}

func (c *Crypto) Max(writer http.ResponseWriter, request *http.Request) {
	_, err := handlers.ExtractUser(request)
	if err != nil {
		c.ErrorBadRequest(writer, err)
		return
	}
	out := c.crypto.Max(request.Context())
	if out.ErrorCode != errors.NoError {
		c.OutputJSON(writer, CryptoResponse{
			ErrorCode: out.ErrorCode,
			Data: Data{
				Message: "retrieving user error",
			},
		})
		return
	}

	c.OutputJSON(writer, CryptoResponse{
		Success:   true,
		ErrorCode: out.ErrorCode,
		Data: Data{
			Crypto: out.Coins,
		},
	})
}

func (c *Crypto) Min(writer http.ResponseWriter, request *http.Request) {
	_, err := handlers.ExtractUser(request)
	if err != nil {
		c.ErrorBadRequest(writer, err)
		return
	}
	out := c.crypto.Min(request.Context())
	if out.ErrorCode != errors.NoError {
		c.OutputJSON(writer, CryptoResponse{
			ErrorCode: out.ErrorCode,
			Data: Data{
				Message: "retrieving user error",
			},
		})
		return
	}

	c.OutputJSON(writer, CryptoResponse{
		Success:   true,
		ErrorCode: out.ErrorCode,
		Data: Data{
			Crypto: out.Coins,
		},
	})
}

func (c *Crypto) Avg(writer http.ResponseWriter, request *http.Request) {
	_, err := handlers.ExtractUser(request)
	if err != nil {
		c.ErrorBadRequest(writer, err)
		return
	}
	out := c.crypto.Avg(request.Context())
	if out.ErrorCode != errors.NoError {
		c.OutputJSON(writer, CryptoResponse{
			ErrorCode: out.ErrorCode,
			Data: Data{
				Message: "retrieving user error",
			},
		})
		return
	}

	c.OutputJSON(writer, CryptoResponse{
		Success:   true,
		ErrorCode: out.ErrorCode,
		Data: Data{
			Crypto: out.Coins,
		},
	})
}
