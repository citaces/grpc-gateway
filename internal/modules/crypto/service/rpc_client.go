package service

import (
	"context"
	"log"
	"net/rpc"

	"gitlab.com/citaces/grpc-gateway/internal/models"
)

type CryptoRPCClient struct {
	client *rpc.Client
}

func NewCryptoRPCClient(client *rpc.Client) *CryptoRPCClient {
	return &CryptoRPCClient{client: client}
}

func (c CryptoRPCClient) Update(ctx context.Context, models models.Coins) {
	err := c.client.Call("CryptoServiceRPC.Update", struct{}{}, nil)
	if err != nil {
		log.Fatal(err)
	}
}

func (c CryptoRPCClient) Cost(ctx context.Context) CostOut {
	var result CostOut
	err := c.client.Call("CryptoServiceRPC.Cost", CostOut{}, &result)
	if err != nil {
		log.Fatal(err)
	}
	return result
}

func (c CryptoRPCClient) History(ctx context.Context) HistoryOut {
	var result HistoryOut
	err := c.client.Call("CryptoServiceRPC.History", CostOut{}, &result)
	if err != nil {
		log.Fatal(err)
	}
	return result
}

func (c CryptoRPCClient) Max(ctx context.Context) MaxOut {
	var result MaxOut
	err := c.client.Call("CryptoServiceRPC.Max", CostOut{}, &result)
	if err != nil {
		log.Fatal(err)
	}
	return result
}

func (c CryptoRPCClient) Min(ctx context.Context) MinOut {
	var result MinOut
	err := c.client.Call("CryptoServiceRPC.Min", CostOut{}, &result)
	if err != nil {
		log.Fatal(err)
	}
	return result
}

func (c CryptoRPCClient) Avg(ctx context.Context) AvgOut {
	var result AvgOut
	err := c.client.Call("CryptoServiceRPC.Avg", CostOut{}, &result)
	if err != nil {
		log.Fatal(err)
	}
	return result
}
