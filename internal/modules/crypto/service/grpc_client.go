package service

import (
	"context"
	"log"

	"google.golang.org/protobuf/types/known/emptypb"

	"gitlab.com/citaces/grpc-gateway/grpc/proto"
	"gitlab.com/citaces/grpc-gateway/internal/infrastructure/errors"
	"gitlab.com/citaces/grpc-gateway/internal/models"
)

type CryptoServiceGRPCClient struct {
	client proto.CryptoServiceRPCClient
}

func NewCryptoServiceGRPCClient(client proto.CryptoServiceRPCClient) *CryptoServiceGRPCClient {
	a := &CryptoServiceGRPCClient{client: client}
	return a
}

func (c *CryptoServiceGRPCClient) Update(ctx context.Context, coins models.Coins) {
	_, err := c.client.Update(ctx, &emptypb.Empty{})
	if err != nil {
		log.Fatal(err)
	}
}

func (c *CryptoServiceGRPCClient) Cost(ctx context.Context) CostOut {
	req, err := c.client.Cost(ctx, &proto.CostRequest{})
	if err != nil {
		return CostOut{
			Coins:     nil,
			ErrorCode: errors.CoinServiceRetrieveCostErr,
		}
	}
	res := make([]models.Coin, len(req.GetCoins()))
	for i, e := range req.GetCoins() {
		res[i] = models.Coin{
			ID:        e.Id,
			Name:      e.Name,
			BuyPrice:  e.BuyPrice,
			SellPrice: e.SellPrice,
			LastTrade: e.LastTrade,
			High:      e.High,
			Low:       e.Low,
			Avg:       e.Avg,
			Vol:       e.Vol,
			VolCurr:   e.VolCurr,
			Updated:   e.Updated,
		}
	}
	return CostOut{
		Coins:     res,
		ErrorCode: int(req.GetErrorCode()),
	}
}

func (c *CryptoServiceGRPCClient) History(ctx context.Context) HistoryOut {
	req, err := c.client.History(ctx, &proto.HistoryRequest{})
	if err != nil {
		return HistoryOut{
			Coins:     nil,
			ErrorCode: errors.CoinServiceRetrieveCostErr,
		}
	}
	res := make([]models.Coin, len(req.GetCoins()))
	for i, e := range req.GetCoins() {
		res[i] = models.Coin{
			ID:        e.Id,
			Name:      e.Name,
			BuyPrice:  e.BuyPrice,
			SellPrice: e.SellPrice,
			LastTrade: e.LastTrade,
			High:      e.High,
			Low:       e.Low,
			Avg:       e.Avg,
			Vol:       e.Vol,
			VolCurr:   e.VolCurr,
			Updated:   e.Updated,
		}
	}
	return HistoryOut{
		Coins:     res,
		ErrorCode: int(req.GetErrorCode()),
	}
}

func (c *CryptoServiceGRPCClient) Max(ctx context.Context) MaxOut {
	req, err := c.client.Max(ctx, &proto.MaxRequest{})
	if err != nil {
		return MaxOut{
			Coins:     nil,
			ErrorCode: errors.CoinServiceRetrieveCostErr,
		}
	}
	res := make([]models.Coin, len(req.GetCoins()))
	for i, e := range req.GetCoins() {
		res[i] = models.Coin{
			ID:        e.Id,
			Name:      e.Name,
			BuyPrice:  e.BuyPrice,
			SellPrice: e.SellPrice,
			LastTrade: e.LastTrade,
			High:      e.High,
			Low:       e.Low,
			Avg:       e.Avg,
			Vol:       e.Vol,
			VolCurr:   e.VolCurr,
			Updated:   e.Updated,
		}
	}
	return MaxOut{
		Coins:     res,
		ErrorCode: int(req.GetErrorCode()),
	}
}

func (c *CryptoServiceGRPCClient) Min(ctx context.Context) MinOut {
	req, err := c.client.Min(ctx, &proto.MinRequest{})
	if err != nil {
		return MinOut{
			Coins:     nil,
			ErrorCode: errors.CoinServiceRetrieveCostErr,
		}
	}
	res := make([]models.Coin, len(req.GetCoins()))
	for i, e := range req.GetCoins() {
		res[i] = models.Coin{
			ID:        e.Id,
			Name:      e.Name,
			BuyPrice:  e.BuyPrice,
			SellPrice: e.SellPrice,
			LastTrade: e.LastTrade,
			High:      e.High,
			Low:       e.Low,
			Avg:       e.Avg,
			Vol:       e.Vol,
			VolCurr:   e.VolCurr,
			Updated:   e.Updated,
		}
	}
	return MinOut{
		Coins:     res,
		ErrorCode: int(req.GetErrorCode()),
	}
}

func (c *CryptoServiceGRPCClient) Avg(ctx context.Context) AvgOut {
	req, err := c.client.Avg(ctx, &proto.AvgRequest{})
	if err != nil {
		return AvgOut{
			Coins:     nil,
			ErrorCode: errors.CoinServiceRetrieveCostErr,
		}
	}
	res := make([]models.Coin, len(req.GetCoins()))
	for i, e := range req.GetCoins() {
		res[i] = models.Coin{
			ID:        e.Id,
			Name:      e.Name,
			BuyPrice:  e.BuyPrice,
			SellPrice: e.SellPrice,
			LastTrade: e.LastTrade,
			High:      e.High,
			Low:       e.Low,
			Avg:       e.Avg,
			Vol:       e.Vol,
			VolCurr:   e.VolCurr,
			Updated:   e.Updated,
		}
	}
	return AvgOut{
		Coins:     res,
		ErrorCode: int(req.GetErrorCode()),
	}
}
