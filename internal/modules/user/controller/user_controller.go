package controller

import (
	"net/http"

	"github.com/ptflp/godecoder"
	"gitlab.com/citaces/grpc-gateway/internal/infrastructure/component"
	"gitlab.com/citaces/grpc-gateway/internal/infrastructure/errors"
	"gitlab.com/citaces/grpc-gateway/internal/infrastructure/handlers"
	"gitlab.com/citaces/grpc-gateway/internal/infrastructure/responder"
	"gitlab.com/citaces/grpc-gateway/internal/modules/user/service"
)

type Userer interface {
	Profile(http.ResponseWriter, *http.Request)
}

type User struct {
	service service.Userer
	responder.Responder
	godecoder.Decoder
}

func NewUser(service service.Userer, components *component.Components) Userer {
	return &User{service: service, Responder: components.Responder, Decoder: components.Decoder}
}

func (u *User) Profile(w http.ResponseWriter, r *http.Request) {
	claims, err := handlers.ExtractUser(r)
	if err != nil {
		u.ErrorBadRequest(w, err)
		return
	}
	out := u.service.GetByID(r.Context(), service.GetByIDIn{UserID: claims.ID})
	if out.ErrorCode != errors.NoError {
		u.OutputJSON(w, ProfileResponse{
			ErrorCode: out.ErrorCode,
			Data: Data{
				Message: "retrieving user error",
			},
		})
		return
	}

	u.OutputJSON(w, ProfileResponse{
		Success:   true,
		ErrorCode: out.ErrorCode,
		Data: Data{
			User: *out.User,
		},
	})
}
