package modules

import (
	"gitlab.com/citaces/grpc-gateway/internal/infrastructure/component"
	acontroller "gitlab.com/citaces/grpc-gateway/internal/modules/auth/controller"
	aservice "gitlab.com/citaces/grpc-gateway/internal/modules/auth/service"
	ccontroller "gitlab.com/citaces/grpc-gateway/internal/modules/crypto/controller"
	"gitlab.com/citaces/grpc-gateway/internal/modules/crypto/service"
	ucontroller "gitlab.com/citaces/grpc-gateway/internal/modules/user/controller"
	uservice "gitlab.com/citaces/grpc-gateway/internal/modules/user/service"
)

type Controllers struct {
	Auth   acontroller.Auther
	User   ucontroller.Userer
	Crypto ccontroller.Crypter
}

func NewControllers(crypto *service.CryptoRPCClient, auth *aservice.AuthServiceJSONRPC, users *uservice.UserServiceJSONRPC, components *component.Components) *Controllers {

	authController := acontroller.NewAuth(auth, components)
	userController := ucontroller.NewUser(users, components)
	cryptoController := ccontroller.NewCrypto(crypto, components)

	return &Controllers{
		Auth:   authController,
		User:   userController,
		Crypto: cryptoController,
	}
}

func NewGRPCControllers(crypto *service.CryptoServiceGRPCClient, auth *aservice.AuthServiceGRPC, users *uservice.UserServiceGRPC, components *component.Components) *Controllers {

	authController := acontroller.NewAuth(auth, components)
	userController := ucontroller.NewUser(users, components)
	cryptoController := ccontroller.NewCrypto(crypto, components)

	return &Controllers{
		Auth:   authController,
		User:   userController,
		Crypto: cryptoController,
	}
}
