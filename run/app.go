package run

import (
	"context"
	"fmt"
	"net/http"
	"net/rpc/jsonrpc"
	"os"

	rabbit "gitlab.com/citaces/grpc-gateway/notifier/cmd/app"

	"gitlab.com/citaces/grpc-gateway/internal/modules/crypto/service"

	"google.golang.org/grpc/credentials/insecure"

	"gitlab.com/citaces/grpc-gateway/grpc/proto"
	"google.golang.org/grpc"

	aservice "gitlab.com/citaces/grpc-gateway/internal/modules/auth/service"
	uservice "gitlab.com/citaces/grpc-gateway/internal/modules/user/service"

	jsoniter "github.com/json-iterator/go"
	"github.com/ptflp/godecoder"
	"gitlab.com/citaces/grpc-gateway/config"
	"gitlab.com/citaces/grpc-gateway/internal/infrastructure/component"
	"gitlab.com/citaces/grpc-gateway/internal/infrastructure/errors"
	"gitlab.com/citaces/grpc-gateway/internal/infrastructure/responder"
	"gitlab.com/citaces/grpc-gateway/internal/infrastructure/router"
	"gitlab.com/citaces/grpc-gateway/internal/infrastructure/server"
	internal "gitlab.com/citaces/grpc-gateway/internal/infrastructure/service"
	"gitlab.com/citaces/grpc-gateway/internal/infrastructure/tools/cryptography"
	"gitlab.com/citaces/grpc-gateway/internal/modules"
	"gitlab.com/citaces/grpc-gateway/internal/provider"
	"go.uber.org/zap"
	"golang.org/x/sync/errgroup"
)

// Application - интерфейс приложения
type Application interface {
	Runner
	Bootstraper
}

// Runner - интерфейс запуска приложения
type Runner interface {
	Run() int
}

// Bootstraper - интерфейс инициализации приложения
type Bootstraper interface {
	Bootstrap(options ...interface{}) Runner
}

// App - структура приложения
type App struct {
	conf   config.AppConf
	logger *zap.Logger
	srv    server.Server
	Sig    chan os.Signal
}

// NewApp - конструктор приложения
func NewApp(conf config.AppConf, logger *zap.Logger) *App {
	return &App{conf: conf, logger: logger, Sig: make(chan os.Signal, 1)}
}

// Run - запуск приложения
func (a *App) Run() int {
	// на русском
	// создаем контекст для graceful shutdown
	ctx, cancel := context.WithCancel(context.Background())

	errGroup, ctx := errgroup.WithContext(ctx)

	// запускаем горутину для graceful shutdown
	// при получении сигнала SIGINT
	// вызываем cancel для контекста
	errGroup.Go(func() error {
		sigInt := <-a.Sig
		a.logger.Info("signal interrupt recieved", zap.Stringer("os_signal", sigInt))
		cancel()
		return nil
	})

	// запускаем http сервер
	errGroup.Go(func() error {
		err := a.srv.Serve(ctx)
		if err != nil && err != http.ErrServerClosed {
			a.logger.Error("app: server error", zap.Error(err))
			return err
		}
		return nil
	})

	if err := errGroup.Wait(); err != nil {
		return errors.GeneralError
	}

	return errors.NoError
}

// Bootstrap - инициализация приложения
func (a *App) Bootstrap() Runner {
	var controllers *modules.Controllers
	// на русском
	// инициализация емейл провайдера
	email := provider.NewEmail(a.conf.Provider.Email, a.logger)
	// инициализация сервиса нотификации
	notifyEmail := internal.NewNotify(a.conf.Provider.Email, email, a.logger)
	// инициализация менеджера токенов
	tokenManager := cryptography.NewTokenJWT(a.conf.Token)
	// инициализация декодера
	decoder := godecoder.NewDecoder(jsoniter.Config{
		EscapeHTML:             true,
		SortMapKeys:            true,
		ValidateJsonRawMessage: true,
		DisallowUnknownFields:  true,
	})
	// инициализация менеджера ответов сервера
	responseManager := responder.NewResponder(decoder, a.logger)
	// инициализация генератора uuid
	uuID := cryptography.NewUUIDGenerator()
	// инициализация хешера
	hash := cryptography.NewHash(uuID)
	// инициализация компонентов
	components := component.NewComponents(a.conf, notifyEmail, tokenManager, responseManager, decoder, hash, a.logger)
	err := rabbit.RabbitMQ(context.Background(), a.logger)
	if err != nil {
		a.logger.Fatal("error init RabbitMQ consumer", zap.Error(err))
	}
	switch a.conf.RPCServer.Type {
	case `RPC`:
		userClient, err := jsonrpc.Dial("tcp", fmt.Sprintf("%s:%s", a.conf.UserRPC.Host, a.conf.UserRPC.Port))
		if err != nil {
			a.logger.Fatal("error init rpc client", zap.Error(err))
		}
		a.logger.Info("rpc client user connected")
		userClientRPC := uservice.NewUserServiceJSONRPC(userClient)

		authClient, err := jsonrpc.Dial("tcp", fmt.Sprintf("%s:%s", a.conf.AuthRPC.Host, a.conf.AuthRPC.Port))

		if err != nil {
			a.logger.Fatal("error init rpc client", zap.Error(err))
		}
		a.logger.Info("rpc client auth connected")
		authClientRPC := aservice.NewAuthServiceJSONRPC(authClient)

		cryptoClient, err := jsonrpc.Dial("tcp", fmt.Sprintf("%s:%s", a.conf.CryptoRPC.Host, a.conf.CryptoRPC.Port))
		if err != nil {
			a.logger.Fatal("error init rpc client", zap.Error(err))
		}
		a.logger.Info("rpc client crypto connected")

		cryptoClientRPC := service.NewCryptoRPCClient(cryptoClient)

		controllers = modules.NewControllers(cryptoClientRPC, authClientRPC, userClientRPC, components)
	case `GRPC`:
		userClient, err := grpc.Dial(fmt.Sprintf("%s:%s", a.conf.UserRPC.Host, a.conf.UserRPC.Port), grpc.WithTransportCredentials(insecure.NewCredentials()))
		if err != nil {
			a.logger.Fatal("error init grpc client", zap.Error(err))
		}
		a.logger.Info("grpc client user connected")
		userClientGRPC := proto.NewUserServiceRPCClient(userClient)

		authClient, err := grpc.Dial(fmt.Sprintf("%s:%s", a.conf.AuthRPC.Host, a.conf.AuthRPC.Port), grpc.WithTransportCredentials(insecure.NewCredentials()))

		if err != nil {
			a.logger.Fatal("error init grpc client", zap.Error(err))
		}
		a.logger.Info("grpc client auth connected")
		authClientGRPC := proto.NewAuthServiceRPCClient(authClient)

		cryptoClient, err := grpc.Dial(fmt.Sprintf("%s:%s", a.conf.CryptoRPC.Host, a.conf.CryptoRPC.Port), grpc.WithTransportCredentials(insecure.NewCredentials()))

		if err != nil {
			a.logger.Fatal("error init grpc client", zap.Error(err))
		}
		a.logger.Info("grpc client exchange connected")
		cryptoClientGRPC := proto.NewCryptoServiceRPCClient(cryptoClient)

		newAuthClientGRPC := aservice.NewAuthServiceGRPC(authClientGRPC)
		newUserClientGRPC := uservice.NewUserServiceGRPC(userClientGRPC)
		newCryptoClientGRPC := service.NewCryptoServiceGRPCClient(cryptoClientGRPC)

		controllers = modules.NewGRPCControllers(newCryptoClientGRPC, newAuthClientGRPC, newUserClientGRPC, components)

	default:
		panic(`wrong rpc server type, fix .env file`)
	}
	// инициализация роутера
	r := router.NewRouter(controllers, components)
	// конфигурация сервера
	srv := &http.Server{
		Addr:    fmt.Sprintf(":%s", a.conf.Server.Port),
		Handler: r,
	}
	// инициализация сервера
	a.srv = server.NewHttpServer(a.conf.Server, srv, a.logger)
	// возвращаем приложение
	return a
}
