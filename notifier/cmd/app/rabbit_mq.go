package rabbit

import (
	"context"

	"go.uber.org/zap"

	"github.com/streadway/amqp"
)

func RabbitMQ(ctx context.Context, logger *zap.Logger) error {
	done := make(chan struct{})

	conn, err := amqp.Dial("amqp://guest:guest@rabbit1:5672/")
	if err != nil {
		logger.Error("Failed to connect to RabbitMQ")
	}

	ch, err := conn.Channel()
	if err != nil {

		logger.Error("Failed to open a channel")
	}
	q, err := ch.QueueDeclare(
		"Crypto_upd", // name
		false,        // durable
		false,        // delete when unused
		false,        // exclusive
		false,        // no-wait
		nil,          // arguments
	)
	if err != nil {

		logger.Error("Failed to declare a queue")
	}
	msgs, err := ch.Consume(
		q.Name, // queue
		"",     // consumer
		true,   // auto-ack
		false,  // exclusive
		false,  // no-local
		false,  // no-wait
		nil,    // args
	)
	if err != nil {

		logger.Error("Failed to register a consumer")
	}
	go func() {
		for d := range msgs {
			logger.Info("Received a message: %s", zap.String(string(d.Body), ``))
		}
	}()
	logger.Info(" [*] Waiting for messages. To exit press CTRL+C")

	select {
	case <-ctx.Done():
		// handle context cancellation
	case <-done:
		// handle completion of work
	}

	return nil
}
